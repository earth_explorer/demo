from django.shortcuts import render
from django.contrib.auth import login
from django.http import HttpResponse
from . import xmpp_auth
from .forms import AuthForm
from django.conf import settings

LOG_FILE = settings.LOG_FILE

def index(request):
    return render(request, 'game/index.html')


def xmpp_authentification(request):
    xb = xmpp_auth.XmppBackend()
    transaction_id = None
    status_msg = ""
    if request.method == 'POST':
        try:
            transaction_id = request.session.get('transaction_id')
        except KeyError:
            request.session['user_logged_in'] = False
            return render(request, 'game/fail.html')
        form = AuthForm(request.POST)
        # check whether it's valid:
        if form.is_valid():
            username = form.cleaned_data['username']
            # Log users
            with open(LOG_FILE, 'a') as lf:
                msg = username + '\n'
                lf.write(msg)
            user, status_code = xb.authenticate(username=username, password=None, transaction_id=transaction_id)
            if user is not None:
                login(request, user)

                # Redirect to a success page.
                request.session['user_logged_in'] = True
                return render(request, 'game/success.html', {'user': user})
            if status_code == 401:
                request.session['user_logged_in'] = False
                status_msg = "User {} refused to authenticate.".format(username)
        else:
            request.session['user_logged_in'] = False
            return render(request, 'game/fail.html')
    else:
        request.session['user_logged_in'] = False
        transaction_id = xb.id_generator(6)
        request.session['transaction_id'] = transaction_id
        form = AuthForm()

    return render(request, 'game/registration/login.html', {'form': form , 'transaction_id' : transaction_id,
                                                       'status_msg': status_msg})
