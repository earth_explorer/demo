from django import forms

class AuthForm(forms.Form):
    username = forms.CharField(max_length=100, help_text="(XMPP jid)")
