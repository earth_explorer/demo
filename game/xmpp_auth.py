import sys
import requests
import string
import random
from django.contrib.auth.models import User


class XmppBackend(object):
    """
    Authenticate with the XMPP 00-70 XEP
    """
    def __init__(self):
        self.transaction_id = None

    def get_transaction_id(self):
        return self.transaction_id
    def set_transaction_id(self, transaction_id):
        self.transaction_id = transaction_id


    def authenticate(self, username=None, password=None, transaction_id = None):
        #  TODO : stocker l'ID dans la BDD
        # Check the token and return a user.
        timeout = 300
        payload = {'jid': username, 'domain': 'demo.agayon.be', 'method': 'POST', 'timeout': timeout,
                   'transaction_id': transaction_id}
        r = requests.get('https://auth.agayon.be/auth', params=payload)
        if r.status_code == 200:
            try:
                user = User.objects.get(username=username)
            except User.DoesNotExist:
                # Create a new user. There's no need to set a password
                user = User(username=username)
                user.is_staff = False
                user.is_superuser = False
                user.save()
            return user, r.status_code
        if r.status_code == 401:
            print("User {} refused to authenticate".format(username), file=sys.stdout)
            return None, r.status_code
        return None, r.status_code

    def get_user(self, user_id):
        try:
            return User.objects.get(pk=user_id)
        except User.DoesNotExist:
            return None, None



    def id_generator(self, size=6, chars=string.ascii_letters + string.digits):
        self.transaction_id = ''.join(random.choice(chars) for _ in range(size))
        return self.transaction_id
