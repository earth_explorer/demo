from django.urls import path
from django.contrib.auth.views import LoginView,  LogoutView
from . import views

urlpatterns = [
    path(r'', views.index, name='index'),
    path(r'login/', views.xmpp_authentification, name='login'),
    path(r'logout/', LogoutView.as_view(template_name='game/registration/logged_out.html'), name='logout')

]
