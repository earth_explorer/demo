# This Django demo website aims to provide a working example of authentication without password using XMPP.

This website has been created in order to illustrate the following article:   

See [https://blog.agayon.be/xmpp_auth_django.html](https://blog.agayon.be/xmpp_auth_django.html)  

In order to use it, you need to rename and adapt the file `earth_explorer/credentials.example.py` to `earth_explorer/credentials.py`.

In this simple version, the authentification URL is harcoded to `https://auth.agayon.be/auth` in the file `xmpp_auth.py`.

As [agayon.be](https://agayon.be) do not intent to authenticate everybody, use it only for testing purpose.

Thanks

